<?php

class Dashboard extends CI_Controller
{
    public function index()
    {
        $this->load->view('dashboard');
    }

   public function createsolution()
   {
       $this->load->view('createsolution');
   }


   public function newslist()
   {
   	$this->load->view('newslist');
   }


   public function editsolution()
   {
   	$this->load->view('editsolution');
   }

// News sections start here ----------------------------------------------------

   public function news()
   {
   	$this->load->view('news');
   }

   public function createnews()
   {
   	$this->load->view('createnews');
   }



	public function testup()
	{
		$this->load->view('test');
	}
}
