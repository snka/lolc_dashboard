<?php


class Login extends CI_Controller
{
    public function login_user()
    {

        $this->form_validation->set_rules('email', 'Email','required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('index');
        }
        else
        {
            {
                $this->load->model('Model_user');
                $result = $this->Model_user->check_user();
                if($result != false)
                {
                    $user_data = array(
                        'userid' => $result->id,
                        'fname' => $result->fname,
                        'lname' => $result->lname,
                        'email' => $result->email,
                        'loggedin' => TRUE
                    );

                    $this->session->set_userdata($user_data);


                    $this->session->set_flashdata('welcome','Welcome Back!');
                    redirect('dashboard/index');

                }
                else
                {
                    $this->session->set_flashdata('loginerr','Wrong username and Password');
                    redirect('home/index');
                }
            }


        }



    }



        public function logoutuser()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('fname');
        $this->session->unset_userdata('lname');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('loggedin');

        redirect('Home/index');
    }

}