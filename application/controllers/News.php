<?php

class News extends CI_Controller
{

	public function newsget($ID)
	{
		$this->load->model('Model_news');
		$data['Newsdata'] = $this->Model_news->news_get($ID);
		$this->load->view('editnews', $data);

	}



	public function updatenews()
	{
		$post_image = $this->input->post('oldpostimg');
		$news_image =  $this->input->post('oldnewsimg');



		$ID = $this->input->post('id');


			// Upload Image one
			$config1['upload_path'] = './uploads/news/featured/';
			$config1['allowed_types'] = 'gif|jpg|png|jpeg';
			$config1['max_size'] = '2048';
			$config1['max_width'] = '2000';
			$config1['max_height'] = '2000';

			// Upload Image two
			$config2['upload_path'] = './uploads/news/';
			$config2['allowed_types'] = 'gif|jpg|png|jpeg';
			$config2['max_size'] = '2048';
			$config2['max_width'] = '2000';
			$config2['max_height'] = '2000';


			if (isset($_FILES['postimg']) && $_FILES['postimg']['name'] != '') {
				$this->upload->initialize($config1);
				$this->load->library('upload', $config1);

				if (!$this->upload->do_upload('postimg')) {
					$errors = array('error' => $this->upload->display_errors());

					$post_image =$this->input->post('oldpostimg');

				} else {
					$data = array('upload_data' => $this->upload->data());
					$post_image = $_FILES['postimg']['name'];
				}

			}

			if (isset($_FILES['newsimg']) && $_FILES['newsimg']['name'] != '') {
				$this->upload->initialize($config2);
				$this->load->library('upload', $config2);

				if (!$this->upload->do_upload('newsimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$news_image =$this->input->post('oldnewsimg');


				} else {
					$data = array('upload_data' => $this->upload->data());
					$news_image = $_FILES['newsimg']['name'];
				}
			}


			$this->load->model('Model_news');
			$this->Model_news->update_news($post_image, $news_image,$ID);
			$this->session->set_flashdata('post_update', 'Your NEWS has been Updated');

			$this->getnews();
		}








































	public function createnews()
	{
		$data['title'] = 'Create Post';

		$this->form_validation->set_rules('title', 'Title', 'required');
		//	$this->form_validation->set_rules('writeup', 'Writeup', 'required');
		//	$this->form_validation->set_rules('fimg', 'Fimg', 'required');
		//	$this->form_validation->set_rules('bimg', 'Bimg', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		//	$this->form_validation->set_rules('dorder', 'Display order', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('createnews');

		} else {
			// Upload Image one
			$config1['upload_path'] = './uploads/news/featured';
			$config1['allowed_types'] = 'gif|jpg|png|jpeg';
			$config1['max_size'] = '2048';
			$config1['max_width'] = '2000';
			$config1['max_height'] = '2000';

			// Upload Image two
			$config2['upload_path'] = './uploads/news';
			$config2['allowed_types'] = 'gif|jpg|png|jpeg';
			$config2['max_size'] = '2048';
			$config2['max_width'] = '2000';
			$config2['max_height'] = '2000';

			if (isset($_FILES['postimg']) && $_FILES['postimg']['name'] != '') {
				$this->upload->initialize($config1);
				$this->load->library('upload', $config1);

				if (!$this->upload->do_upload('postimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$post_image = 'noimage.jpg';
				} else {
					$data = array('upload_data' => $this->upload->data());
					$post_image = $_FILES['postimg']['name'];
				}

			}

			if (isset($_FILES['newsimg']) && $_FILES['newsimg']['name'] != '') {
				$this->upload->initialize($config2);
				$this->load->library('upload', $config2);

				if (!$this->upload->do_upload('newsimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$news_image = 'noimage.jpg';

				} else {
					$data = array('upload_data' => $this->upload->data());
					$news_image = $_FILES['newsimg']['name'];
				}
			}


			$this->load->model('Model_news');
			$this->Model_news->create_news($post_image, $news_image);
			$this->session->set_flashdata('post_created', 'Your post has been created');
			$this->load->view('createnews');
		}
	}


	public function getnews()
	{
		$this->load->model('Model_news');
		$data['newslist'] = $this->Model_news->get_news();
		$this->load->view('news', $data);

	}

	public function deletenews($id)
	{

		if ($id) {

			$this->load->model('Model_news');
			$delete = $this->Model_news->delete_news($id);

			if ($delete) {
				$this->session->set_flashdata('deletemsg', 'News has been removed successfully.');


			} else {
				$this->session->set_flashdata('deletemsg', 'Some problems occurred, please try again.');

			}
		}


	}


}
