<?php

class Posts extends CI_Controller
{


	public function newsolution()
	{

		$data['title'] = 'Create Post';

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('writeup', 'Writeup', 'required');

		//	$this->form_validation->set_rules('fimg', 'Fimg', 'required');
		//	$this->form_validation->set_rules('bimg', 'Bimg', 'required');

		$this->form_validation->set_rules('content', 'Content', 'required');

		$this->form_validation->set_rules('dorder', 'Display order', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('createnewsolution');

		} else {
			// Upload Image one
			$config1['upload_path'] = './uploads/solutions/';
			$config1['allowed_types'] = 'gif|jpg|png|jpeg';
			$config1['max_size'] = '2048';
			$config1['max_width'] = '2000';
			$config1['max_height'] = '2000';

			// Upload Image two
			$config2['upload_path'] = './uploads/solution_sub_page/';
			$config2['allowed_types'] = 'gif|jpg|png|jpeg';
			$config2['max_size'] = '2048';
			$config2['max_width'] = '2000';
			$config2['max_height'] = '2000';

			// Upload Image three
			$config3['upload_path'] = './uploads/';
			$config3['allowed_types'] = 'gif|jpg|png|jpeg';
			$config3['max_size'] = '2048';
			$config3['max_width'] = '2000';
			$config3['max_height'] = '2000';

			if (isset($_FILES['fimg']) && $_FILES['fimg']['name'] != '') {
				$this->upload->initialize($config1);
				$this->load->library('upload', $config1);

				if (!$this->upload->do_upload('fimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$feature_image = 'noimage.jpg';
				} else {
					$data = array('upload_data' => $this->upload->data());
					$feature_image = $_FILES['fimg']['name'];
				}

			}

			if (isset($_FILES['bimg']) && $_FILES['bimg']['name'] != '') {
				$this->upload->initialize($config2);
				$this->load->library('upload', $config2);

				if (!$this->upload->do_upload('bimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$banner_image = 'noimage.jpg';

				} else {
					$data = array('upload_data' => $this->upload->data());
					$banner_image = $_FILES['bimg']['name'];
				}
			}

			if (isset($_FILES['limg']) && $_FILES['limg']['name'] != '') {
				$this->upload->initialize($config3);
				$this->load->library('upload', $config3);

				if (!$this->upload->do_upload('limg')) {
					$errors = array('error' => $this->upload->display_errors());
					$logo_image = 'noimage.jpg';

				} else {
					$data = array('upload_data' => $this->upload->data());
					$logo_image = $_FILES['limg']['name'];
				}
			}

			$this->load->model('Model_post');

			$this->Model_post->create_solution($feature_image, $banner_image, $logo_image);

			$this->session->set_flashdata('post_created', 'Your post has been created');
			$this->load->view('createsolution');
		}
	}

	public function updatesolution()
	{
		// $data['title'] = 'Edit Post';
		$ID = $this->input->post('id');
		$feature_image = $this->input->post('oldfimg');
		$banner_image = $this->input->post('oldbimg');
		$logo_image = $this->input->post('oldlimg');


		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('writeup', 'Writeup', 'required');
		//	$this->form_validation->set_rules('fimg', 'Fimg', 'required');
		//	$this->form_validation->set_rules('bimg', 'Bimg', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		$this->form_validation->set_rules('dorder', 'Display order', 'required');


		if ($this->form_validation->run() === FALSE) {
			$slug = url_title($this->input->post('title'));
			$data['Postsdata'] = array(
				'id' => $this->input->post('id'),
				'title' => $this->input->post('title'),
				'url' => $slug,
				'content' => $this->input->post('content'),
				'writeup' => $this->input->post('writeup'),

				'status' => $this->input->post('status'),
				'display_order' => $this->input->post('dorder'),
				'added_date' => $this->input->post('date'),

				'featured_image' => $this->input->post('fimg'),
				'banner_image' => $this->input->post('bimg'),
				'logo_image' => $this->input->post('limg'),
			);
			$this->load->view('editnews', $data);

		} else {
			// Upload Image one
			$config1['upload_path'] = './uploads/solutions/';
			$config1['allowed_types'] = 'gif|jpg|png|jpeg';
			$config1['max_size'] = '2048';
			$config1['max_width'] = '2000';
			$config1['max_height'] = '2000';

			// Upload Image two
			$config2['upload_path'] = './uploads/solution_sub_page/';
			$config2['allowed_types'] = 'gif|jpg|png|jpeg';
			$config2['max_size'] = '2048';
			$config2['max_width'] = '2000';
			$config2['max_height'] = '2000';

			// Upload Image three
			$config3['upload_path'] = './uploads/';
			$config3['allowed_types'] = 'gif|jpg|png|jpeg';
			$config3['max_size'] = '2048';
			$config3['max_width'] = '2000';
			$config3['max_height'] = '2000';

			if (isset($_FILES['fimg']) && $_FILES['fimg']['name'] != '') {
				$this->upload->initialize($config1);
				$this->load->library('upload', $config1);

				if (!$this->upload->do_upload('fimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$feature_image = $this->input->post('oldfimg');

				} else {
					$data = array('upload_data' => $this->upload->data());
					$feature_image = $_FILES['fimg']['name'];
				}

			}else{}

			if (isset($_FILES['bimg']) && $_FILES['bimg']['name'] != '') {
				$this->upload->initialize($config2);
				$this->load->library('upload', $config2);

				if (!$this->upload->do_upload('bimg')) {
					$errors = array('error' => $this->upload->display_errors());
					$banner_image = $this->input->post('oldbimg');


				} else {
					$data = array('upload_data' => $this->upload->data());
					$banner_image = $_FILES['bimg']['name'];
				}
			}else{}

			if (isset($_FILES['limg']) && $_FILES['limg']['name'] != '') {
				$this->upload->initialize($config3);
				$this->load->library('upload', $config3);

				if (!$this->upload->do_upload('limg')) {
					$errors = array('error' => $this->upload->display_errors());
					$logo_image = $this->input->post('oldlimg');


				} else {
					$data = array('upload_data' => $this->upload->data());
					$logo_image = $_FILES['limg']['name'];
				}
			}else{}



			$this->load->model('Model_post');
			$res = $this->Model_post->update_post($feature_image, $banner_image, $logo_image, $ID);
			if ($res) {

				$this->getsolutions();
			}
		}
	}





	public function getsolution($ID)
	{
		$this->load->model('Model_post');
		$data['Postsdata'] = $this->Model_post->get_solution($ID);
		$this->load->view('editsolution', $data);

	}

	public function getsolutions()
	{
		$this->load->model('Model_post');
		$data['posts'] = $this->Model_post->get_solutions();
		$this->load->view('solutions', $data);

	}

	public function deletesolution($id)
	{

		if ($id) {

			$this->load->model('Model_post');
			$delete = $this->Model_post->delete_solution($id);

			if ($delete) {
				$this->session->set_flashdata('deletemsg', 'Post has been removed successfully.');
				$this->getsolutions();


			} else {
				$this->session->set_flashdata('deletemsg', 'Some problems occurred, please try again.');
				$this->getsolutions();

			}
		}


	}


}
