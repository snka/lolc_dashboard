<?php

class Register extends CI_Controller
{
    public function index()
    {
        $this->load->view('register');
    }

    public function register_user()
    {
        $this->form_validation->set_rules('fname', 'First name', 'required');
        $this->form_validation->set_rules('lname', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[users.email]');

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password1', 'Password1', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('register');
        }
        else
        {
             $this->load->model('Model_user');
           $result =  $this->Model_user->insert_data();

           if($result)
           {
               $this->session->set_flashdata('msg','done!');
               redirect('Register/index');
           }
           else
               {
                   $this->session->set_flashdata('msg','not done!');
                   redirect('Register/index');

               }

        }


    }
}