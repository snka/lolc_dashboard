<?php


class Model_news extends CI_Model
{
	//update the news
	public function update_news($post_image,$news_image,$id)
	{


		$data = array(
			'news_title' => $this->input->post('title'),
			'news_content' => $this->input->post('content'),
			'news_date' => $this->input->post('newsdate'),
			'status' => $this->input->post('status'),
			'added_date' => $this->input->post('addeddate'),

			'news_image' => $post_image,
		);

		$this->db->where('news_id', $id);
		$this->db->update('tblnews', $data);



		$data1 = array(

			'image_desc' => $this->input->post('newsimgdec'),
			'image_name' => $news_image,
			'added_date' => $this->input->post('addeddate'),



		);



		$this->db->where('news_id', $id);
		$this->db->update('tblnews_image', $data1);
		return $this->db->affected_rows();


	}


















	//Get Data to Fill Edit Form
	public function news_get($id)
	{
		$this->db->select('tblnews.*,tblnews_image.*');
		$this->db->from('tblnews');
		$this->db->join('tblnews_image', 'tblnews.news_id = tblnews_image.news_id', 'left');
		$this->db->where('tblnews.news_id', $id);
		$query = $this->db->get();
		return $query->result();

	}


	//Get News For Table
	public function get_news()
	{
		$this->db->from('tblnews');
		$query = $this->db->get();
		return $query->result();
	}

	public function create_news($post_image, $news_image)
	{

		$data = array(
			'news_title' => $this->input->post('title'),
			'news_content' => $this->input->post('content'),
			'news_date' => $this->input->post('newsdate'),
			'status' => $this->input->post('status'),
			'added_date' => $this->input->post('addeddate'),
			'news_image' => $post_image,
		);

		$this->db->insert('tblnews', $data);
		$id = $this->db->insert_id();


		$data1 = array(
			'news_id' => $id,
			'image_desc' => $this->input->post('newsimgdec'),
			'image_name' => $news_image,
			'added_date' => $this->input->post('addeddate'),


		);

		return $this->db->insert('tblnews_image', $data1);
	}

	public function get_news_fimg($id)
	{
		$this->db->from('tblnews');
		$this->db->where('news_id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_news_cimg($id)
	{
		$this->db->from('tblnews_image');
		$this->db->where('news_id', $id);
		$query = $this->db->get();
		return $query->row();

	}


	public function delete_news($id)
	{

		$imges1 = $this->get_news_fimg($id);
		$news_image = $imges1->news_image;


		$delete = $this->db->delete('tblnews', array('news_id' => $id));
		if ($delete) {
			unlink("./uploads/news/featured/" . $news_image);

			$imges2 = $this->get_news_cimg($id);
			$image_name = $imges2->image_name;
			$delete = $this->db->delete('tblnews_image', array('news_id' => $id));
			if ($delete) {
				unlink("./uploads/news/" . $image_name);

				return true;
			}


		} else {
			return false;
		}


	}


}
