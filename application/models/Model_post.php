<?php


class Model_post extends CI_Model
{
	// Solution Methords Start here

	public function create_solution($feature_image, $banner_image, $logo_image)
	{
		$slug = url_title($this->input->post('title'));
		$data = array(
			'title' => $this->input->post('title'),
			'url' => $slug,
			'content' => $this->input->post('content'),
			'writeup' => $this->input->post('writeup'),

			'status' => $this->input->post('status'),
			'display_order' => $this->input->post('dorder'),
			'added_date' => $this->input->post('date'),

			'featured_image' => $feature_image,
			'banner_image' => $banner_image,
			'logo_image' => $logo_image,

		);
		return $this->db->insert(' tblsolutions', $data);
	}

	public function update_post($feature_image, $banner_image, $logo_image, $id)
	{
		$slug = url_title($this->input->post('title'));
		$data = array(
			'title' => $this->input->post('title'),
			'url' => $slug,
			'content' => $this->input->post('content'),
			'writeup' => $this->input->post('writeup'),

			'status' => $this->input->post('status'),
			'display_order' => $this->input->post('dorder'),
			'added_date' => $this->input->post('date'),

			'featured_image' => $feature_image,
			'banner_image' => $banner_image,
			'logo_image' => $logo_image,
		);

		$this->db->where('id', $id);
		$this->db->update('tblsolutions', $data);
		return $this->db->affected_rows();

	}

	public function get_solution($id)
	{
		$this->db->from('tblsolutions');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_solutions()
	{
		$this->db->from('tblsolutions');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_solution($id)
	{
		$imges =$this->get_solution($id);

		$imgf = $imges->featured_image;
		$imgb = $imges->banner_image;
		$imgl = $imges->logo_image;

		$delete = $this->db->delete('tblsolutions', array('id' => $id));
		if($delete)
		{
			unlink("./uploads/solutions/".$imgf );
			unlink("./uploads/solution_sub_page/".$imgb );
			unlink("./uploads/".$imgl );

			return true;
		}
		else
		{
			return false;
		}

//		return $delete ? true : false;
	}







}
