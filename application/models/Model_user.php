<?php


class Model_user extends CI_Model
{
    public function insert_data()
    {
        $data = array(
            'fname' => $this->input->post('fname',TRUE),
            'lname' => $this->input->post('lname',TRUE),
            'email' => $this->input->post('email',TRUE),
            'password' => sha1($this->input->post('password',TRUE)),

        );

       return  $this->db->insert('users',$data);
    }


    public function check_user()
    {
        $username =$this->input->post('email');
        $password =  sha1($this->input->post('password'));

        $this->db->where('email',$username);
        $this->db->where('password',$password);
        $resp = $this->db->get('users');

        if($resp->num_rows() == 1)
        {
            return $resp->row(0);

        }
        else
        {
            return false;
        }
    }


}