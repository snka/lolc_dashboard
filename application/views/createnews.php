<?php include('include/dashboard/header.php'); ?>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
<body>


<script type="text/javascript">


	if (window.history.replaceState) {
		window.history.replaceState(null, null, window.location.href);
	}

	$(document).ready(function () {
		var data = "<?php echo $this->session->flashdata('post_created') ?>";
		console.log(data);
		if (data) {
			console.log(data);
			$.bootstrapGrowl(
				'<?php echo $this->session->flashdata('post_created') ?>',
				{
					type: 'success',
					delay: 3000,
					offset: {from: 'top', amount: 70},
				});

		} else {

		}
	});


</script>


<div class="container-fluid">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
			<li class="breadcrumb-item active" aria-current="page">Create News</li>
		</ol>
	</nav>

	<!-- form  start -->
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">

					<div class="container">

						<?php echo validation_errors(); ?>
						<?php echo form_open_multipart('News/createnews'); ?>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="inputEmail4">Title</label>
								<input type="text" class="form-control" id="title" placeholder="News Title"
									   name="title" required/>
							</div>

						</div>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="inputAddress">Content</label>
								<textarea rows="5" type="text" class="form-control" id="inputAddress"
										  placeholder="HTML CODE" name="content" required></textarea>
							</div>

						</div>

						<div class="form-row">
							<div class="form-group col-md-5">
								<label for="inputCity">News Date</label>
								<input   value=" " type="text" class="form-control"
									   name="newsdate" id="datepicker" required />
							</div>
							<div class="form-group col-md-5">
								<label for="inputCity">Added Date</label>
								<input readonly value="<?php echo date("Y-m-d"); ?>" type="text" class="form-control"
									   id="inputCity" name="addeddate" required />
							</div>
							<div class="form-group col-md-2">
								<label for="inputZip">Status</label>
								<input value="1" type="text" class="form-control" id="inputZip" name="status" required />
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-5">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="validatedCustomFile"
										   name="postimg" >
									<label class="custom-file-label" for="validatedCustomFile">Post Image</label>

								</div>
							</div>

						</div>
						<div class="form-row">
							<div class="form-group col-md-5">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="validatedCustomFile"
										   name="newsimg" >
									<label class="custom-file-label" for="validatedCustomFile">News Image</label>

								</div>
							</div>
						</div>
						<div class="form-row">
						<div class="form-group col-md-5">
								<label for="inputAddress">News Image Description</label>
								<textarea rows="2" type="text" class="form-control" id="inputAddress"
										  placeholder=" " name="newsimgdec" required></textarea>
							</div>
						</div>

						<button type="submit" class="btn btn-primary">Create</button>
						</form>
					</div>

					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- form end -->

</div>
</body>
<!-- data picker script -->
<script>
	$('#datepicker').datepicker({

		uiLibrary: 'bootstrap4',
		format: 'yyyy-mm-dd'
	});

</script>

<?php include('include/dashboard/footer.php'); ?>
<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>

<link rel="stylesheet"
	  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css"/>
