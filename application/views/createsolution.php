<?php include('include/dashboard/header.php'); ?>

<body>



<script type="text/javascript">


	if ( window.history.replaceState ) {
		window.history.replaceState( null, null, window.location.href );
	}

		$(document).ready(function () {
			var data = "<?php echo $this->session->flashdata('post_created') ?>";
			console.log(data);
			if (data) {
				console.log(data);
				$.bootstrapGrowl(
					'<?php echo $this->session->flashdata('post_created') ?>',
					{
						type: 'success',
					delay: 3000,
						offset: {from: 'top', amount: 70},
				});

			} else {

			}
		});




</script>





<div class="container-fluid">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
			<li class="breadcrumb-item active" aria-current="page">Create News</li>
		</ol>
	</nav>
	 <!--<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">

					<div class="container">

						<?php /*echo validation_errors(); */?>
						<?php /*echo form_open_multipart('Posts/create'); */?>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" name="title" placeholder="Add Title">
						</div>
						<div class="form-group">
							<label>Body</label>
							<textarea id="editor1" class="form-control" name="body" placeholder="Add Body"></textarea>
						</div>

						<div class="form-group">
							<label>Upload Image</label>
							<input type="file" name="userfile" size="20">
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
						</form>

					</div>
				</div>
			</div>
		</div>

	</div>-->
	<!-- test start -->
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">

					<div class="container">

						<?php echo validation_errors(); ?>

						<?php echo form_open_multipart('Posts/newsolution'); ?>
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="inputEmail4">Title</label>
									<input type="text" class="form-control" id="title" placeholder="Solution Title"
										   name="title">
								</div>

							</div>
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="inputEmail4">Writeup</label>
									<textarea id="writeup" class="form-control" placeholder="Writeup"
											  name="writeup"></textarea>
								</div>

							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="validatedCustomFile"
											   name="fimg" >
										<label class="custom-file-label" for="validatedCustomFile">Feature Image</label>

									</div>
								</div>
								<div class="form-group col-md-4">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="validatedCustomFile"
											   name="bimg" >
										<label class="custom-file-label" for="validatedCustomFile">Banner Image</label>

									</div>
								</div>
								<div class="form-group col-md-4">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="validatedCustomFile"
											   name="limg" >
										<label class="custom-file-label" for="validatedCustomFile">Logo Image</label>

									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="inputAddress">Content</label>
								<textarea rows="5" type="text" class="form-control" id="inputAddress"
										  placeholder="HTML CODE" name="content"></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-8">
									<label for="inputCity">Date</label>
									<input value="<?php echo date("Y-m-d"); ?>" type="text" class="form-control"
										   id="inputCity" name="date">
								</div>
								<div class="form-group col-md-2">
									<label for="inputZip">Status</label>
									<input value="1" type="text" class="form-control" id="inputZip" name="status" >
								</div>
								<div class="form-group col-md-2">
									<label for="inputZip">Display Order</label>
									<input value="1" type="text" class="form-control" id="dorder" name="dorder">
								</div>
							</div>

							<button type="submit" class="btn btn-primary">Create</button>
						</form>


					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- test end -->

</div>
</body>


<?php include('include/dashboard/footer.php'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
