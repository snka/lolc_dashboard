<?php include('include/dashboard/header.php'); ?>

<body>
<div class="container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit solution</li>
        </ol>
    </nav>


    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="container">

                        <?php echo validation_errors(); ?>
                        <?php echo form_open_multipart('Posts/updatesolution'); ?>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Title</label>
                                <input value="<?php echo $Postsdata->title ?>" type="text" class="form-control"
                                       id="title" placeholder="Solution Title"
                                       name="title" required />

                                <input hidden value="<?php echo $Postsdata->id ?>"   name="id" />
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Writeup</label>
                                <textarea id="writeup" class="form-control" placeholder="Writeup"
                                          name="writeup" required><?php echo $Postsdata->writeup ?></textarea>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <div class="custom-file">
									<input hidden value="<?php echo $Postsdata->featured_image ?>" name="oldfimg" />
                                    <input  type="file"
                                           class="custom-file-input" id="validatedCustomFile"
                                           name="fimg" />
                                    <label class="custom-file-label" for="validatedCustomFile">Feature Image</label>

                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="custom-file">
									<input hidden value="<?php echo $Postsdata->banner_image ?>" name="oldbimg" />
                                    <input  type="file"
                                           class="custom-file-input" id="validatedCustomFile"
                                           name="bimg" />
                                    <label class="custom-file-label" for="validatedCustomFile">Banner Image</label>

                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="custom-file">
									<input hidden value="<?php echo $Postsdata->logo_image ?>" name="oldlimg" />
                                    <input  type="file"
                                           class="custom-file-input" id="validatedCustomFile"
                                           name="limg" />
                                    <label class="custom-file-label" for="validatedCustomFile">Logo Image</label>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputAddress">Content</label>
                            <textarea rows="5" type="text" class="form-control" id="inputAddress"
                                      placeholder="HTML CODE"
                                      name="content" required><?php echo $Postsdata->content ?></textarea>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="inputCity">Date</label>
                                <input value="<?php echo $Postsdata->added_date ?>" type="text" class="form-control"
                                       id="inputCity" name="date" readonly />
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputZip">Status</label>
                                <input value="<?php echo $Postsdata->status ?>" type="text" class="form-control"
                                       id="inputZip" name="status" required />
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputZip">Display Order</label>
                                <input value="<?php echo $Postsdata->display_order ?>" type="text" class="form-control"
                                       id="dorder" name="dorder" required />
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Create</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


</body>


<?php include('include/dashboard/footer.php'); ?>
