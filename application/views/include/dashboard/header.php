<?php
if (!($this->session->userdata('loggedin'))) {
    redirect('Home/index');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


    <link href="<?php echo base_url('assests/css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assests/css/sidebar.css') ?>" rel="stylesheet">


</head>

<div id="wrapper">

    <nav class="navbar header-top fixed-top navbar-expand-lg  navbar-dark bg-dark">
        <a class="navbar-brand" href="#"><img src="https://www.lolcgeneral.com/images/logo.png" width="50%"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav side-nav">
                <li>
                    <a href="<?php echo base_url('dashboard/index') ?>" style="">
                        <i class="fa fa-home fa-lg"></i>&nbsp; DASHBOARD
                    </a>
                </li>
                <br>
                <li data-toggle="collapse" data-target="#products" class="collapsed active">
                    <a href="#"><i class="fa fa-folder-open-o fa-lg"></i>&nbsp; NEWS <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="<?php echo base_url('news/getnews') ?>">News</a></li>
                    <li><a href="<?php echo base_url('dashboard/createnews') ?>">Create News</a></li>
                   <!-- <li><a href="#">Edit News</a></li>-->

                </ul>


                <li data-toggle="collapse" data-target="#solutions" class="collapsed active">
                    <a href="#"><i class="fa fa-folder-open-o fa-lg"></i>&nbsp; SOLUTIONS <span
                                class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="solutions">
                    <li class="active"><a href="<?php echo base_url('posts/getsolutions') ?>">Solutions</a></li>
                    <li><a href="<?php echo base_url('dashboard/createsolution') ?>">Create Solution</a></li>
                  <!--  <li><a href="#">Edit Solutions</a></li>-->

                </ul>

            </ul>

            </ul>

            <ul class="navbar-nav ml-md-auto d-md-flex">


                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('login/logoutuser') ?>"><i
                                class="fa fa-power-off fa-lg"></i>&nbsp;&nbsp;&nbsp;<?php echo $this->session->userdata('fname') ?>
                    </a>
                </li>


            </ul>
        </div>
    </nav>



