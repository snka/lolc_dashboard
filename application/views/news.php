<?php include('include/dashboard/header.php'); ?>


<body>
<div class="container-fluid">
	<nav aria-label="breadcrumb">
		<ol style="background-color: transparent;margin: 0px;  padding: 10px" class="breadcrumb">
			<li class="breadcrumb-item"><a style="text-decoration: none;color: #4F5155" href="#">Dashboard</a></li>
			<li class="breadcrumb-item active" aria-current="page"><a style="text-decoration: none;color: #4F5155"
																	  href="#">Solutions List</a></li>
		</ol>
	</nav>
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">

					<table class="table">
						<thead>
						<tr>
							<th width="5%" scope="col">ID</th>
							<th width="40%" scope="col">News Title</th>
							<!--	<th scope="col">Content</th>-->
							<th scope="col">Image</th>
							<th scope="col">Status</th>
							<th scope="col">News Date</th>
							<th scope="col">Added Date</th>
							<th colspan="2" scope="col">Actions</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($newslist as $news) { ?>
							<tr>
								<td scope="row"><?php echo $news->news_id ?></td>
								<td width="180px"><?php echo $news->news_title ?></td>
								<!--<td width="350px"> <?php /*echo $news->news_content */ ?></td>-->

								<td width="350px"> <?php echo $news->news_image ?></td>

								<td><?php

									$s = $news->status;
									if ($s == 1) {
										echo "<span class=\"badge badge-success\">Enable</span>";
									} else {
										echo "<span class=\"badge badge-danger\">Disable</span>";

									}
									?>


								</td>
								<td><?php echo $news->news_date ?></td>
								<td><?php echo $news->added_date ?></td>
								<td>
									<a style="padding-right: 7px" class="editbtn"
									   data-id="<?php echo $news->news_id ?>">
										<button type="button" class="btn btn-default btn-sm btn-warning"
												data-toggle="modal"
												data-target="#editModal"><i class="fa fa-pencil" aria-hidden="true"></i>
										</button>
									</a>
									<a class="deletebtn" data-id="<?php echo $news->news_id ?>">
										<button type="button" data-toggle="modal"
												data-target="#deleteModal" class="btn btn-default btn-sm btn-danger"><i
												class="fa fa-trash-o" aria-hidden="true"></i>
										</button>
									</a>


								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<!-- Edit js start -->
<script>
	$('.editbtn').click(function () {
		var ID = $(this).data('id');
		$('#confirm-edit').data('id', ID); //set the data attribute on the modal button


		$('#confirm-edit').click(function () { //check model confirm
			var ID = $(this).data('id');
			console.log(ID);

			window.location.href = "<?php echo base_url(); ?>index.php/News/newsget/" + ID,
				$.ajax({
					//url: "<?php echo base_url(); ?>index.php/Posts/editpost/" + ID,
					//type: "post",
					//data: ID,

				});
		});
	});
</script>
<!-- Edit js end -->

<!-- Edit Modal Start -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div style="text-align: center" class="modal-content">
			<div style="border: 0px;padding-bottom: 0px; padding-top: 3px" class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img style="display: block;   margin-left: auto;   margin-right: auto;   "
					 src="https://png.icons8.com/color/96/000000/box-important.png">
				<h5 style="font-family: 'Montserrat', sans-serif">Are you Sure?</h5>
				<p style="color: darkgray">Do you really want this? This process cannot be undone!.</p>
			</div>
			<div class="text-center" style="border: 0px;padding-bottom: 15px">

				<button style="padding: 8px 25px 8px 25px;margin-right: 6px" type="button" class="btn btn-secondary"
						data-dismiss="modal">No
				</button>

				<button style="color:whitesmoke;padding: 8px 25px 8px 25px;" type="button" class="btn btn-warning"
						id="confirm-edit">
					Yes
				</button>
			</div>
		</div>
	</div>
</div>
<!-- edit Modal End-->
<!-- nortification js start -->
<script type="text/javascript">
	if (window.history.replaceState) {
		window.history.replaceState(null, null, window.location.href);
	}
	$(document).ready(function () {
		var data = "<?php echo $this->session->flashdata('deletemsg') ?>";
		console.log(data);
		if (data) {

			$.bootstrapGrowl(
				'<?php echo $this->session->flashdata('deletemsg') ?>',
				{
					type: 'success',
					delay: 3000,
					offset: {from: 'top', amount: 70},
				});

		} else {

		}
	});
</script>
<!-- nortification js ends -->

<!-- delete js start-->

<script>
	$('.deletebtn').click(function () {
		var ID = $(this).data('id');
		$('#confirm-delete').data('id', ID); //set the data attribute on the modal button
		$('#confirm-delete').click(function () { //check model confirm
			var ID = $(this).data('id');
			event.preventDefault();
			jQuery.noConflict();
			$.ajax({
				url: "<?php echo base_url(); ?>index.php/News/deletenews/" + ID,
				type: "post",
				data: ID,
				success: function (data) {
					$('#deleteModal').modal('hide');
					location.reload();
					console.log("DONE");
				}
			});
		});
	});
</script>


<!-- delete js end -->

<!-- delete Modal Start -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div style="text-align: center" class="modal-content">
			<div style="border: 0px;padding-bottom: 0px; padding-top: 3px" class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img style="display: block;   margin-left: auto;   margin-right: auto;   "
					 src="https://png.icons8.com/color/100/000000/cancel.png" width="20%">
				<h5 style="font-family: 'Montserrat', sans-serif">Are you Sure?</h5>
				<p style="color: darkgray">Do you really want this? This process cannot be undone!.</p>
			</div>
			<div class="text-center" style="border: 0px;padding-bottom: 15px">
				<button style="padding: 8px 25px 8px 25px;margin-right: 6px" type="button" class="btn btn-secondary"
						data-dismiss="modal"> No
				</button>
				<button style="color:whitesmoke;padding: 8px 25px 8px 25px;background-color:#EB3F3F" type="button"
						class="btn btn-danger" id="confirm-delete"> Delete
				</button>
			</div>
		</div>
	</div>
</div>
<!--delete Modal End-->

</body>


<?php include('include/dashboard/footer.php'); ?>
<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
