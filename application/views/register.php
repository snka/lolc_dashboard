<?php include('include/dashboard/header.php') ?>


    <div class="container">
        <?php
        if($this->session->flashdata('msg'))
        {
            echo"<h3>".$this->session->flashdata('msg')."</h3>";
        }


        ?>

        <div class="col-md-8">
            <?php echo validation_errors(); ?>
            <?php echo form_open('Register/register_user'); ?>
            <div class="form-group">
                <label class="control-label col-sm-2" for="fname">First Name:</label>
                <div class="col-sm-10">
                    <input   class="form-control" id="email" placeholder="first name" name="fname">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Last Name:</label>
                <div class="col-sm-10">
                    <input   class="form-control" id="email" placeholder="last name" name="lname">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Password:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Password again:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password1">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Register</button>
                </div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>





    </body>
<?php include ('include/dashboard/footer.php');